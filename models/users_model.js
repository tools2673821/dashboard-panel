const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const usersModel = new Schema({
  name: {
    type: String,
    required: true,
  },
  username: {
    type: String,
    required: [true, "Username harus diisi"],
    unique: [true, "Username sudah digunakna"],
  },
  email: {
    type: String,
    required: [true, "Email harus diisi"],
    unique: [true, "Email sudah digunakna"],
  },
  password: {
    type: String,
    required: [true, "Password harus diisi"],
  },
  kode_merchant: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: "Merchant",
  },
  role: {
    type: String,
    default: 'mitra' // mitra / admin
  },
  aktif: {
    type: Boolean,
  },
});

module.exports = exports = mongoose.model("User", usersModel, "users");
