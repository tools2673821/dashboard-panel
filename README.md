# Payuni Dashbaord Addon Wa Bot


# Installation and Configuration

### Configuration

- setting up database configuration in /config/db.json
- setting up socket configuration in /config/socket.json

### Running using docker-compose

```bash
$ git clone https://gitlab.com/tools2673821/dashboard-panel.git
$ cd dashboard-panel
$ docker-compose up -d --build
```

## Credits
Built with Nodejs v14

## Docs

### Create Instance
- Click Tambah Instances
- Input Nama Instance
- Input your Wehbook URL
- Generate QR
- Scan QRcode

### Menus
#### GenerateQr
- for re-generate qrcode

#### Close Instance
- close the session properly to ensure the session is saved for the next time you log in

#### Delete
- delete instance and bot session

#### Edit
- edit your instance data


### Send Chat
- user send new chat
- tools check on message
- tools hit webhook and sendback the message using webhook
---

Copyright ©️ 2023 by PT. Fintek Digital Nusantara