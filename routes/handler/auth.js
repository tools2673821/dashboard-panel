const usersModel = require("../../models/users_model");
const bcrypt = require("bcrypt");

function Auth() {
  this.index = async (req, res) => {
    if (req.session?.user) {
      return res.redirect("/");
    }
    return res.render("pages/signin", {
      title: "Signin WhatsApp Bot",
    });
  };

  this.signup = async (req, res) => {
    if (req.session?.user) {
      return res.redirect("/");
    }
    return res.render("pages/signup", {
      title: "Signup WhatsApp Bot",
    });
  };

  this.validateAuth = async (req, res) => {
    let { email, password } = req.body;
    await usersModel
      .findOne({ $or: [{ email }, { username: email }] })
      .populate("kode_merchant")
      .then(async (user) => {
        if (!user) {
          return res.json({
            status: 404,
            message: "User tidak ditemukan",
          });
        }

        if (!user.aktif) {
          return res.json({
            status: 403,
            message: "Status User tidak aktif",
          });
        }

        if (!user.kode_merchant.aktif) {
          return res.json({
            status: 403,
            message: "Status merchant tidak aktif",
          });
        }

        const isPasswordMatch = await bcrypt.compare(password, user.password);
        if (!isPasswordMatch) {
          return res.json({
            status: 400,
            message: "Password tidak sesuai",
          });
        }

        delete user.password;
        req.session.user = user;
        req.session.save();
        return res.json({
          status: 200,
          message: `Login Success`,
        });
      })
      .catch((error) => {
        console.log("Error : ", error);
        res.json({
          status: 500,
          message: "Internal server error !",
        });
      });
  };

  this.checkAuth = async (req, res, next) => {
    if (req.session.user) {
      usersModel
        .findOne({
          _id: req.session.user._id,
        })
        .populate("kode_merchant")
        .then(async (user) => {
          if (!user) return res.redirect("/signin");
          req.user = user;
          return next();
        });
    } else {
      return res.redirect("/signin");
    }
  };

  this.isAdmin = async (req, res, next) => {
    if (req.user.role == "admin") {
      return next();
    } else {
      return res.redirect("/");
    }
  };

  this.logout = async (req, res) => {
    req.session.destroy();
    res.redirect("/signin");
  };
}

module.exports = exports = Auth;
