const merchantsModel = require("../../models/merchants_model");
const usersModel = require("../../models/users_model");
const deviceModel = require("../../models/devices_model");

const ObjectId = require("mongoose").Types.ObjectId;

function Merchant() {
  this.index = async (req, res) => {
    return res.render("pages/merchant", {
      title: "Manage Merchant",
    });
  };

  this.create = async (req, res) => {
    const source = req.body;

    try {
      const merchant = new merchantsModel(source);
      await merchant.save();

      return res.json({
        status: "success",
        message: "Data berhasil ditambah",
        data: merchant,
      });
    } catch (err) {
      console.log("Error : ", err);
      if (err && err.name === "ValidationError") {
        return res.status(433).json({
          status: "error",
          message: err.message,
        });
      }

      // validate unique field
      if (err && err.name === "MongoServerError") {
        let value = "";
        for (let key in err.keyValue) {
          value = err.keyValue[key];
        }
        return res.status(433).json({
          status: "error",
          message: `Merchant validation failed: ${Object.keys(
            err.keyValue
          )} : ${value} sudah terdaftar`,
        });
      }
    }
  };

  this.list = async (req, res) => {
    let where = {};

    if (req.query.search) {
      where["$or"] = [
        {
          nama_merchant: new RegExp(
            req.query.search["value"].replace(/[.*+?^${}()|[\]\\]/g, ""),
            "gi"
          ),
        },
        {
          email_merchant: new RegExp(
            req.query.search["value"].replace(/[.*+?^${}()|[\]\\]/g, ""),
            "gi"
          ),
        },
      ];
    }

    merchantsModel
      .find(where)
      .skip(parseInt(req.query.start))
      .limit(parseInt(req.query.length))
      .sort({_id: -1})
      .then((result) => {
        let output = result.map((item) => {
          let action = `<div class='dropdown-primary dropdown open'>
                          <button class='btn btn-sm btn-primary dropdown-toggle waves-effect waves-light' id='dropdown-${item._id}' data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                              Aksi
                          </button>
                          <div class='dropdown-menu' aria-labelledby="dropdown-${item._id}" data-dropdown-out='fadeOut'>
                              <a class='dropdown-item' onclick='return getData("${item._id}");' href='javascript:void(0)' title='Edit'>Edit</a>
                              <a class='dropdown-item' onclick='return removeData("${item._id}");' href='javascript:void(0)' title='Remove'>Hapus</a>
                          </div>
                      </div>`;

          let status =
            item.aktif == true
              ? '<button class="badge badge-success">Active</button>'
              : '<button class="badge badge-danger">Closed</button>';
          return {
            action,
            status,
            name: item.nama_merchant,
            email: item.email_merchant,
          };
        });

        merchantsModel.countDocuments(where).then((total) => {
          return res.json({
            draw: req.query.draw,
            recordsFiltered: total,
            recordsTotal: total,
            data: output,
          });
        });
      });
  };

  this.detail = async (req, res) => {
    try {
      const id = req.params.id;
      const merchant = await merchantsModel.findOne({ _id: id });
      if (!merchant) {
        return res.status(404).json({
          status: "error",
          message: "Data tidak ditemukan",
        });
      }

      return res.json({
        status: "success",
        data: merchant,
      });
    } catch (error) {
      return res.status(400).json({
        status: "error",
        message: error.message,
      });
    }
  };

  this.delete = async (req, res) => {
    try {
      const id = req.body.id;
      const merchant = await merchantsModel.findOne({ _id: id });
      if (!merchant) {
        return res.status(404).json({
          status: "error",
          message: "Data tidak ditemukan",
        });
      }

      const checkUserExist = await usersModel.find({
        kode_merchant: merchant._id,
      });

      if (checkUserExist.length) {
        return res.status(400).json({
          status: "error",
          message: "Opps.. Merchant masih memiliki user aktif",
        });
      }

      const checkDeviceExist = await deviceModel.find({
        kode_merchant: merchant._id,
      });

      if (checkDeviceExist.length) {
        return res.status(400).json({
          status: "error",
          message: "Opps.. Merchant masih memiliki instance aktif",
        });
      }

      await merchantsModel.findOneAndRemove({ _id: id });
      return res.json({
        status: "success",
        message: "Data berhasil dihapus",
      });
    } catch (error) {
      return res.status(400).json({
        status: "error",
        message: error.message,
      });
    }
  };

  this.update = async (req, res) => {
    try {
      const { id, nama_merchant, aktif, email_merchant, logo } = req.body;
      const merchant = await merchantsModel.findOne({ _id: new ObjectId(id) });
      if (!merchant) {
        return res.status(404).json({
          status: "error",
          message: "Data tidak ditemukan",
        });
      }

      await merchantsModel.findOneAndUpdate(
        { _id: id },
        {
          nama_merchant,
          aktif,
          email_merchant,
          logo,
        }
      );
      return res.json({
        status: "success",
        message: "Data berhasil diperbarui",
      });
    } catch (error) {
      return res.status(400).json({
        status: "error",
        message: error.message,
      });
    }
  };
}

module.exports = exports = Merchant;
