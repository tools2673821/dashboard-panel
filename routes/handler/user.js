const usersModel = require("../../models/users_model");
const merchantsModel = require("../../models/merchants_model");
const ObjectId = require("mongoose").Types.ObjectId;

const bcrypt = require("bcrypt");

function User() {
  this.index = async (req, res) => {
    let where = {
      aktif: true,
    };
    const merchants = await merchantsModel.find(where);
    return res.render("pages/user", {
      title: "Manage User",
      merchants,
    });
  };

  this.create = async (req, res) => {
    const source = req.body;
    try {
      let { name, username, email, password, role, kode_merchant } = source;
      if (!name || !email || !password || !role) {
        return res.status(400).json({
          status: "error",
          message: "Data belum lengkap",
        });
      }

      if (kode_merchant && kode_merchant != "all") {
        const checkMerchant = await merchantsModel.findOne({
          _id: new ObjectId(kode_merchant),
          aktif: true,
        });
        if (!checkMerchant) {
          return res.status(404).json({
            status: "error",
            message: "Merchant tidak ditemukan",
          });
        }
      } else {
        kode_merchant = req.session.user.kode_merchant._id;
      }

      const payload = {
        name,
        email,
        username,
        password: bcrypt.hashSync(source.password, 10),
        kode_merchant: new ObjectId(kode_merchant),
        aktif: source.aktif ? (source.aktif == "true" ? true : false) : true,
      };

      const user = new usersModel(payload);
      await user.save();

      return res.json({
        status: "success",
        message: "Data berhasil ditambah",
      });
    } catch (err) {
      console.log("Error : ", err);
      if (err && err.name === "ValidationError") {
        return res.status(433).json({
          status: "error",
          message: err.message,
        });
      }

      // validate unique field
      if (err && err.name === "MongoServerError") {
        let value = "";
        for (let key in err.keyValue) {
          value = err.keyValue[key];
        }
        return res.status(433).json({
          status: "error",
          message: `Users validation failed: ${Object.keys(
            err.keyValue
          )} : ${value} sudah terdaftar`,
        });
      }
    }
  };

  this.list = async (req, res) => {
    let where = {};

    if (req.query.search) {
      where["$or"] = [
        {
          username: new RegExp(
            req.query.search["value"].replace(/[.*+?^${}()|[\]\\]/g, ""),
            "gi"
          ),
        },
      ];
    }

    usersModel
      .find(where)
      .populate("kode_merchant")
      .skip(parseInt(req.query.start))
      .limit(parseInt(req.query.length))
      .sort({_id: -1})
      .then((result) => {
        let output = result.map((item) => {
          let action = `<div class='dropdown-primary dropdown open'>
                          <button class='btn btn-sm btn-primary dropdown-toggle waves-effect waves-light' id='dropdown-${item._id}' data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                              Aksi
                          </button>
                          <div class='dropdown-menu' aria-labelledby="dropdown-${item._id}" data-dropdown-out='fadeOut'>
                              <a class='dropdown-item' onclick='return getData("${item._id}");' href='javascript:void(0)' title='Edit'>Edit</a>
                              <a class='dropdown-item' onclick='return removeData("${item._id}");' href='javascript:void(0)' title='Remove'>Hapus</a>
                          </div>
                      </div>`;

          let status =
            item.aktif == true
              ? '<button class="badge badge-success">Active</button>'
              : '<button class="badge badge-danger">Closed</button>';
          let merchant = item.kode_merchant.nama_merchant;
          return {
            action,
            status,
            name: item.name,
            email: item.email,
            username: item.username,
            role: item.role,
            merchant,
          };
        });

        usersModel.countDocuments(where).then((total) => {
          return res.json({
            draw: req.query.draw,
            recordsFiltered: total,
            recordsTotal: total,
            data: output,
          });
        });
      });
  };

  this.detail = async (req, res) => {
    try {
      const id = req.params.id;
      const user = await usersModel.findOne({ _id: id });
      if (!user) {
        return res.status(404).json({
          status: "error",
          message: "Data tidak ditemukan",
        });
      }

      return res.json({
        status: "success",
        data: user,
      });
    } catch (error) {
      return res.status(400).json({
        status: "error",
        message: error.message,
      });
    }
  };

  this.delete = async (req, res) => {
    try {
      const id = req.body.id;
      const user = await usersModel.findOne({ _id: id });
      if (!user) {
        return res.status(404).json({
          status: "error",
          message: "Data tidak ditemukan",
        });
      }

      await usersModel.findOneAndRemove({ _id: id });
      return res.json({
        status: "success",
        message: "Data berhasil dihapus",
      });
    } catch (error) {
      return res.status(400).json({
        status: "error",
        message: error.message,
      });
    }
  };

  this.update = async (req, res) => {
    const source = req.body
    try {
      let { id, name, username, email, role, kode_merchant } = source;
      if ((!id, !name || !email || !role)) {
        return res.status(400).json({
          status: "error",
          message: "Data belum lengkap",
        });
      }

      const user = await usersModel.findOne({ _id: new ObjectId(id) });
      if (!user) {
        return res.status(404).json({
          status: "error",
          message: "Data tidak ditemukan",
        });
      }

      if (kode_merchant && kode_merchant != "all") {
        const checkMerchant = await merchantsModel.findOne({
          _id: new ObjectId(kode_merchant),
          aktif: true,
        });
        if (!checkMerchant) {
          return res.status(404).json({
            status: "error",
            message: "Merchant tidak ditemukan",
          });
        }
      } else {
        kode_merchant = req.session.user.kode_merchant._id;
      }

      const payload = {
        name,
        email,
        username,
        kode_merchant: new ObjectId(kode_merchant),
        aktif: source.aktif ? (source.aktif == "true" ? true : false) : true,
      };

      if (source.password && source.password != "") {
        payload.password = bcrypt.hashSync(source.password, 10);
      }

      await usersModel.findOneAndUpdate(
        {
          _id: id,
        },
        payload
      );

      return res.json({
        status: "success",
        message: "Data berhasil diperbarui",
      });
    } catch (error) {
      return res.status(400).json({
        status: "error",
        message: error.message,
      });
    }
  };
}

module.exports = exports = User;
