const devicesModel = require("../../models/devices_model");
const merchantsModel = require("../../models/merchants_model");
const mongoose = require("mongoose");
const ObjectId = require("mongoose").Types.ObjectId;

function Manage() {
  this.index = async (req, res) => {
    let where = {
      aktif: true,
    };
    const merchants = await merchantsModel.find(where);
    return res.render("pages/manage", {
      title: "Manage Instance",
      merchants,
    });
  };

  this.list = async (req, res) => {
    let where = {};

    if (req.user.role != "admin") {
      where.kode_merchant = req.user.kode_merchant;
    }

    if (req.query.search) {
      let whereId = []
      if(ObjectId.isValid(req.query.search['value'])){
        whereId = [{
          _id: new ObjectId(req.query.search['value'])
        }]
      }
      
      where["$or"] = [
        {
          instanceName: new RegExp(
            req.query.search["value"].replace(/[.*+?^${}()|[\]\\]/g, ""),
            "gi"
          ),
        },
        {
          phone: new RegExp(
            req.query.search["value"].replace(/[.*+?^${}()|[\]\\]/g, ""),
            "gi"
          ),
        },
        ...whereId
      ];
    }

    devicesModel
      .find(where)
      .skip(parseInt(req.query.start))
      .limit(parseInt(req.query.length))
      .sort({_id: -1})
      .populate("kode_merchant")
      .then((result) => {
        let output = result.map((item) => {
          let action = `<div class='dropdown-primary dropdown open'>
                          <button class='btn btn-sm btn-primary dropdown-toggle waves-effect waves-light' id='dropdown-${item._id}' data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                              Aksi
                          </button>
                          <div class='dropdown-menu' aria-labelledby="dropdown-${item._id}" data-dropdown-out='fadeOut'>
                              <a class='dropdown-item' onclick='return generateQR("${item._id}");' href='javascript:void(0)' title='Generate'>Generate QR</a>
                              <a class='dropdown-item' onclick='return getData("${item._id}");' href='javascript:void(0)' title='Edit'>Edit</a>
                              <a class='dropdown-item' onclick='return shuwdown("${item._id}");' href='javascript:void(0)' title='Edit'>Shut Down</a>
                              <a class='dropdown-item' onclick='return removeData("${item._id}");' href='javascript:void(0)' title='Remove'>Hapus</a>
                          </div>
                      </div>`;

          if (req.user.role != "admin") {
            action = `<div class='dropdown-primary dropdown open'>
                            <button class='btn btn-sm btn-primary dropdown-toggle waves-effect waves-light' id='dropdown-${item._id}' data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Aksi
                            </button>
                            <div class='dropdown-menu' aria-labelledby="dropdown-${item._id}" data-dropdown-out='fadeOut'>
                                <a class='dropdown-item' onclick='return generateQR("${item._id}");' href='javascript:void(0)' title='Generate'>Generate QR</a>
                                <a class='dropdown-item' onclick='return shuwdown("${item._id}");' href='javascript:void(0)' title='Edit'>Shut Down</a>
                                <a class='dropdown-item' onclick='return getData("${item._id}");' href='javascript:void(0)' title='Edit'>Edit</a>
                            </div>
                        </div>`;
          }

          let status =
            item.status == 1
              ? '<button class="badge badge-success">Active</button>'
              : '<button class="badge badge-danger">Closed</button>';
          let name = `<small>Name : ${
            item.instanceName
          }</small><br /><small>Merchant: ${
            item.kode_merchant?.nama_merchant || ""
          }</small>`;
          return {
            action,
            name,
            phone: item.phone ?? "",
            status,
            webhook: item.webhook ?? "",
            token: item._id,
            merchantName: item.kode_merchant?.nama_merchant ?? "",
          };
        });

        devicesModel.countDocuments(where).then((total) => {
          return res.json({
            draw: req.query.draw,
            recordsFiltered: total,
            recordsTotal: total,
            data: output,
          });
        });
      });
  };

  this.detail = async (req, res) => {
    try {
      const id = req.params.id;
      const instance = await devicesModel.findOne({ _id: id });
      if (!instance) {
        return res.status(404).json({
          status: "error",
          message: "Data tidak ditemukan",
        });
      }

      return res.json({
        status: "success",
        data: instance,
      });
    } catch (error) {
      return res.status(400).json({
        status: "error",
        message: error.message,
      });
    }
  };

  this.createInstance = async (req, res) => {
    try {
      if (!req.session?.user) {
        return res.status(400).json({
          status: "error",
          message: "Merchant tidak ditemukan",
        });
      }

      let { instanceName, webhook, phone, kode_merchant } = req.body;
      // Jika instance dibuat untuk mitra
      if (kode_merchant && kode_merchant != "all") {
        const checkMerchant = await merchantsModel.findOne({
          _id: kode_merchant,
          aktif: true,
        });
        if (!checkMerchant) {
          return res.status(404).json({
            status: "errpr",
            message: "Merchant tidak ditemukan",
          });
        }
      } else {
        kode_merchant = req.session.user.kode_merchant;
      }

      const newInstance = new devicesModel({
        instanceName,
        webhook,
        phone: phone?.replace(new RegExp("^08"), "628"),
        token: "5ec7ea9caf3760550f8e1dfb",
        kode_merchant,
      });

      await newInstance.save();
      return res.json({
        status: "success",
        message: "Berhasil menambah instance baru",
      });
    } catch (error) {
      return res.status(400).json({
        status: "error",
        message: error.message,
      });
    }
  };

  this.deleteInstance = async (req, res) => {
    try {
      const id = req.body.id;
      const instance = await devicesModel.findOne({ _id: id });
      if (!instance) {
        return res.status(404).json({
          status: "error",
          message: "Data tidak ditemukan",
        });
      }

      await devicesModel.findOneAndRemove({ _id: id });
      return res.json({
        status: "success",
        message: "Data berhasil dihapus",
      });
    } catch (error) {
      return res.status(400).json({
        status: "error",
        message: error.message,
      });
    }
  };

  this.updateInstance = async (req, res) => {
    try {
      let { id, instanceName, webhook, phone, kode_merchant } = req.body;
      const instance = await devicesModel.findOne({ _id: new ObjectId(id) });
      if (!instance) {
        return res.status(404).json({
          status: "error",
          message: "Data tidak ditemukan",
        });
      }

      if (kode_merchant && kode_merchant != "all") {
        const checkMerchant = await merchantsModel.findOne({
          _id: kode_merchant,
          aktif: true,
        });
        if (!checkMerchant) {
          return res.status(404).json({
            status: "errpr",
            message: "Merchant tidak ditemukan",
          });
        }
      } else {
        kode_merchant = req.session.user.kode_merchant;
      }

      await devicesModel.findOneAndUpdate(
        { _id: id },
        {
          instanceName,
          webhook,
          phone: phone?.replace(new RegExp("^08"), "628"),
          kode_merchant,
        }
      );
      return res.json({
        status: "success",
        message: "Data berhasil diperbarui",
      });
    } catch (error) {
      return res.status(400).json({
        status: "error",
        message: error.message,
      });
    }
  };

  this.shutdownInstance = async (req, res) => {
    try {
      const id = req.body.id;
      const instance = await devicesModel.findOne({ _id: id });
      if (!instance) {
        return res.status(404).json({
          status: "error",
          message: "Data tidak ditemukan",
        });
      }

      await devicesModel.findOneAndUpdate({ _id: id }, { status: 0});
      return res.json({
        status: "success",
        message: "Instance berhasil di shutdown",
      });
    } catch (error) {
      return res.status(400).json({
        status: "error",
        message: error.message,
      });
    }
  };
}

module.exports = exports = Manage;
