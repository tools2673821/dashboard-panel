const mode = process.env.NODE_ENV || "dev";
const configSocket = require("../config/socket.json")[mode];
const express = require("express");
const router = express.Router();

const AuthHandler = require("./handler/auth"),
  Auth = new AuthHandler();
const ManageHandler = require("./handler/manage"),
  Manage = new ManageHandler();
const MerchantHandler = require("./handler/merchant"),
  Merchant = new MerchantHandler();
const UserHander = require("./handler/user"),
  User = new UserHander();

router.use((req, res, next) => {
  res.locals.socketUrl = configSocket.url;
  next();
});

// Auth
router.get("/signin", Auth.index);
// router.get("/signup", Auth.signup);
router.get("/signout", Auth.logout);
router.post("/validateAuth", Auth.validateAuth);

// Merchants
// Manage Instances
router.use(Auth.checkAuth);
router.use(function (req, res, next) {
  res.locals.session = req.user;
  next();
});
/* Dashboard Page. */
router.get("/", function (req, res, next) {
  res.render("index", { title: "Payuni" });
});

// Instance
router.get("/manage-instance", Manage.index);
router.get("/manage-instance/list", Manage.list);
router.get("/manage-instance/:id/detail", Manage.detail);
router.post("/manage-instance/create", Manage.createInstance);
router.post("/manage-instance/delete", Manage.deleteInstance);
router.post("/manage-instance/update", Manage.updateInstance);
router.post("/manage-instance/shutdown", Manage.shutdownInstance);

// Merchant
router.use('/merchant', Auth.isAdmin)
router.get("/merchant", Merchant.index);
router.get("/merchant/list", Merchant.list);
router.get("/merchant/:id/detail", Merchant.detail);
router.post("/merchant/create", Merchant.create);
router.post("/merchant/delete", Merchant.delete);
router.post("/merchant/update", Merchant.update);

// Users
router.use('/user', Auth.isAdmin)
router.get("/user", User.index);
router.get("/user/list", User.list);
router.get("/user/:id/detail", User.detail);  
router.post("/user/create", User.create);
router.post("/user/delete", User.delete);
router.post("/user/update", User.update);

module.exports = router;
